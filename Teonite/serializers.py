from rest_framework import serializers
from .models import Autor,Stats,Top10


class StatsSerializer(serializers.ModelSerializer):
    class Meta:
        model=Stats
        fields=('Word','Count')

class AutorSerializer(serializers.ModelSerializer):
    words=StatsSerializer(read_only=True,many=True)
    class Meta:
        model=Autor
        fields=('name','words')

class top10Serializer(serializers.ModelSerializer):
    class Meta:
        model=Top10
        fields=('T10Word','T10Count')

class SubAutorSerializer(serializers.ModelSerializer):
    class Meta:
        model=Autor
        fields=('Subname','name')