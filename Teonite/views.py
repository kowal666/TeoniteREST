from django.shortcuts import render
from .models import All_post_links,Autor,Stats,Top10
from .serializers import AutorSerializer,StatsSerializer,top10Serializer,SubAutorSerializer
from rest_framework import viewsets
from .DatabaseCheck import Links_checker

#-------------------SERIALIZERS-----------------------------
class AutorView(viewsets.ModelViewSet):
    queryset = Autor.objects.all()
    serializer_class = AutorSerializer
    lookup_field = "Subname"

class SubAutorView(viewsets.ModelViewSet):
    queryset = Autor.objects.all()
    serializer_class = SubAutorSerializer

class StatsView(viewsets.ModelViewSet):
    serializer_class = StatsSerializer
    queryset = Stats.objects.all()
    def get_queryset(self):
        qs = super(StatsView, self).get_queryset()
        qs = qs.all().order_by("-Count")[::1][:10]
        return qs

class Top10View(viewsets.ModelViewSet):
    queryset = Top10.objects.all()
    serializer_class = top10Serializer
    def get_queryset(self):
        qs2 = super(Top10View, self).get_queryset()
        qs2 = qs2.all().order_by("-T10Count")[::1][:10]
        return qs2
#------------------------------------------------------------

#Check if Database Scrap data
try:
    Autor.objects.get(Subname='MartaKoziel')

except Autor.DoesNotExist:
    Links_checker()
