from django.db import models



class Top10(models.Model):
    T10_word=models.CharField(name='T10Word',max_length=255)
    T10_word_count=models.IntegerField(name="T10Count")

    def __str__(self):
        return self.T10Word + ":" + str(self.T10Count)

# Create your models here.
class All_post_links(models.Model):
    Link=models.CharField(name='Link',max_length=400)
    def __str__(self):
        return self.Link

class Stats(models.Model):

    # Fields
    Word = models.CharField(name='Word',max_length=255)
    Word_count = models.IntegerField(name="Count")

    def __str__(self):
        return self.Word+":"+str(self.Count)


class Autor(models.Model):
    # Fields
    name = models.CharField(max_length=255)
    content=models.TextField(max_length=None)
    sub_name = models.CharField(name='Subname', max_length=255, default='puste',unique=True)
    # Relationship Fields
    words = models.ManyToManyField(Stats)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('-pk',)