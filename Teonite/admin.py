from django.contrib import admin

# Register your models here.
from .models import Autor, Stats,All_post_links,Top10

admin.site.register(Autor)
admin.site.register(Stats)
admin.site.register(All_post_links)
admin.site.register(Top10)